﻿using System.IO;
using iText.Html2pdf;
using iText.Html2pdf.Resolver.Font;
using iText.IO.Font;
using iText.Kernel.Pdf;
using iText.Pdfa;
using iText.Layout.Font;

namespace SUP4648_6
{
    internal class Program
    {
        public static string BASEURI = "..\\..\\..\\resources\\";
        public static string SRC = string.Format("{0}test.html", BASEURI);
        public static string DEST = string.Format("{0}test.pdf", BASEURI);
        public static string INTENT = string.Format("{0}intent.icm", BASEURI);
        public static string FONT = string.Format("{0}TimesRoman.ttf", BASEURI);

        public static void Main(string[] args)
        {
            //pdf-a van maken
            PdfOutputIntent outputIntent = new PdfOutputIntent("Custom", "", "https://www.color.org",
                "sRGB IEC61966-2.1", new FileStream(INTENT, FileMode.Open));
            PdfADocument pdfADocument = new PdfADocument(new PdfWriter(new FileStream(DEST, FileMode.Create)),
                PdfAConformanceLevel.PDF_A_2A, outputIntent);
            pdfADocument.SetTagged();
            
            ConverterProperties cp = new ConverterProperties();
            
            FontProvider fontProvider = new DefaultFontProvider(false, false, false);
            fontProvider.AddFont(FontProgramFactory.CreateFont(FONT, true));
            
            cp.SetFontProvider(fontProvider);
            cp.SetBaseUri(BASEURI);
            
            HtmlConverter.ConvertToPdf(new FileStream(SRC, FileMode.Open), pdfADocument, cp);
        }
    }
}